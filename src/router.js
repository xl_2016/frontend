import Index from './views/index.vue';
import Home from './views/home.vue';
// 登录页面
import Login from './views/login.vue';
// 0001
import view_0001 from './views/0001.vue';
// 0002
import view_0002 from './views/0002.vue';
// backtestData
import backtestData from './views/backtestData.vue';
// analyzeData
import analyzeData from './views/analyzeData.vue';
import analyzeData1 from './views/analyzeData1.vue';

const routers = [
    {
        path: '/login',
        name: 'login',
        meta: {
            title: '菜单1'
        },
        component: Login
    },

    {
        path: '/',
        name: 'index',
        redirect: '/0001',
        meta: {
        },
        component: (resolve) => require(['./views/index.vue'], resolve),
        children: [
        	{
        	  	path: '/home',
        		name: 'home',
        	  	meta: {
        	      	title: '菜单1'
        	  	},
        	  	component: Home
        	},
            // 0001
            {
                path: '/0001',
                name: '0001',
                meta: {
                    title: '0001'
                },
                component: view_0001
            },
            // 0002
            {
                path: '/0002',
                name: '0002',
                meta: {
                    title: '0002'
                },
                component: view_0002
            },
            // 0002 回测数据展示 backtestData
            {
                path: '/backtestData',
                name: 'backtestData',
                meta: {
                    title: '回测'
                },
                component: backtestData
            },
            // 0002 分析数据展示 analyzeData
            {
                path: '/analyzeData',
                name: 'analyzeData',
                meta: {
                    title: '分析'
                },
                component: analyzeData
            },
            // {
            //     path: '/analyzeData1',
            //     name: 'analyzeData1',
            //     meta: {
            //         title: '分析'
            //     },
            //     component: analyzeData1
            // },
            
        ]
    }
];
export default routers;