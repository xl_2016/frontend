import Axios from 'axios'
import Vue from 'vue'
import apiList from './apiList'
import { random } from './common'
import Cookies from 'js-cookie'

// 请求链接添加统一前缀
Axios.defaults.baseURL = '/api'
// 超时时间 毫秒
Axios.defaults.timeout = 600000;

// 设置默认 Content-Type
// Axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'

// 添加请求拦截器
Axios.interceptors.request.use(config => {

    // 请求中添加 version 参数 确保ie浏览器 及时刷新数据而不使用缓存 start
    // if (config.params) {
    //     config.params.version = random(16);
    // } else {
    //     config.params = {
    //         version: random(16)
    //     }
    // }
    // 请求中添加 version 参数 确保ie浏览器 及时刷新数据而不使用缓存 end

    // 转换链接 统一管理在 /libs/apiList 管理列表中不存在的场合直接请求 存在的场合 获取
    if (!apiList[config.url]) {
        config.url = '' + config.url.replace(':userName', Cookies.get('userName'))
    } else {
        var url = apiList[config.url]
        config.url = url.replace(':userName', Cookies.get('userName'));
    }

    console.log('Axios.config', config);

    // 设置请求头
    // config.headers['content-type'] = 'multipart/form-data';

    return config
}, error => {
    // 对请求错误做些什么
    return Promise.reject(error)
})

// 添加响应拦截器
Axios.interceptors.response.use(response => {


    if (response.config.responseType == 'blob') {
        return response;
    }


    // 请求成功的场合 对响应数据做点什么
    return response.data;
}, error => {
    // 对响应错误做点什么
    return Promise.reject(error)
})

export default Axios
