
import $ from 'jquery'

export default {

  // 测试
  test: '/api/owk/test.do',
  // 转介成分列表
  tCpaDtaOrdPctItrManList: '/api/owk/tCpaDtaOrdPctItrManList.do',
  // 转介成分详情
  tCpaDtaOrdPctItrManDetail: '/api/owk/tCpaDtaOrdPctItrManDetail.do',
  // 转介成分编辑
  tCpaDtaOrdPctItrManEdit: '/api/owk/tCpaDtaOrdPctItrManEdit.do',
  // 转介成分新增
  tCpaDtaOrdPctItrManNew: '/api/owk/tCpaDtaOrdPctItrManNew.do',
  // 校验用户
  checkUser: '/api/owk/checkUser.do',
  // 校验用户
  upload: '/api/owk/upload.do',
  // 重点产品系数 列表
  tCpaDtaTstCoeList: '/api/owk/tCpaDtaTstCoeList.do',
  // 默认产品系数 列表
  tFndFeeRatList: '/api/owk/tFndFeeRatList.do',
  // 基金产品系数 列表
  tCpaDtaFndCoeList: '/api/owk/tCpaDtaFndCoeList.do',
  
  // get
  get: function(url, header, params, async, Vue, callback, errorCallback) {
    $.ajax({
      url: this[url] + params,
      // 默认值: true。默认设置下，所有请求均为异步请求。如果需要发送同步请求，请将此选项设置为 false。
      async: async,
      processData: true,
      type: 'get',
      timeout: 60000,
      beforeSend: function(request) {
        if (header) {
          header.forEach(function(element, index) {
            request.setRequestHeader(element[0], element[1]);
          });
        }
      },
      success: function (data, status) {
        callback(data)
      },
      error: function (XHR, status, error) {
        console.log('请求出错：', error)
        if (errorCallback) {
          errorCallback(error);
        }
      }
    })
  },
  // post
  post: function(url, header, params, async, Vue, callback, errorCallback) {
    // 默认值: true。默认设置下，所有请求均为异步请求。如果需要发送同步请求，请将此选项设置为 false。
    $.ajax({
      url: this[url],
      async: async,
      processData: true,
      type: 'POST',
      contentType: 'application/x-www-form-urlencoded',
      data: params,
      timeout: 60000,
      dataType: 'json',
      beforeSend: function(request) {
        if (header) {
          header.forEach(function(element, index) {
            request.setRequestHeader(element[0], element[1]);
          });
        }
      },
      success: function (data, status) {
          callback(data)
      },
      error: function (XHR, status, error) {
        console.log('请求出错：', error)
        if (errorCallback) {
          errorCallback(error);
        }
      }
    })
  },
}