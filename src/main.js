import Vue from 'vue';
import ViewUI from 'view-design';
import VueRouter from 'vue-router';
import Routers from './router';
import Util from './libs/util';
// import service from './libs/service' // jquery 请求方式
import axios from './libs/axios'
import App from './app.vue';
import Vuex from 'vuex';

// import echarts from 'echarts'
// import 'echarts-gl'
import Cookies from 'js-cookie'
import jquery from 'jquery'

import { page, formatDate } from './libs/common'
import 'view-design/dist/styles/iview.css';
import 'babel-polyfill';

Vue.use(VueRouter);
Vue.use(ViewUI);
Vue.use(Vuex);

// 路由配置
const RouterConfig = {
    mode: 'hash',
    routes: Routers
};
const router = new VueRouter(RouterConfig);

const store = new Vuex.Store({
    state: {
        count: 0
    },
    mutations: {
        increment (state) {
            state.count++
        }
    }
});

router.beforeEach((to, from, next) => {
    ViewUI.LoadingBar.start();
    Util.title(to.meta.title);
    next();
});

router.afterEach((to, from, next) => {
    ViewUI.LoadingBar.finish();
    window.scrollTo(0, 0);
});

// 发送请求
Vue.prototype.$http = axios
// 分页
Vue.prototype.$page = page
// 公用方法
Vue.prototype.$formatDate = formatDate
// jquery
// Vue.prototype.$jquery = jquery
// Cookies
Vue.prototype.$cookies = Cookies
// Cookies
// Vue.prototype.$echarts = echarts


new Vue({
    el: '#app',
    router: router,
    store: store,
    render: h => h(App)
});
