
/*
	分页
*/
export const page = function() {
	return {
		pageNumber: 1,
		pageSize: 20,
		totalCount: 0
	}
}

/*
    随机字符串
*/
export const random = (len) => {
　　len = len || 32;
　　var chars = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d'];
　　var maxPos = chars.length;
　　var pwd = '';
    for (var i = 0; i < len; i++) {
        pwd += chars[Math.floor(Math.random()*(0 - 35) + 35)];
    }
　　return pwd;
}


/*
    日期时间格式转换
    兼容 2020-03-26T09:28:25.000+0000 & 时间戳
*/
export const formatDateTime = (date) => {
    if (!date) {
        return '';
    }
    let newDate;
    if (typeof(date) == 'number') {
        newDate = new Date(date);
    } else {
        // 2020-03-26T09:28:25.000+0000
        // 2020-03-26T17:28:25.000+0800
        // 针对后台特殊格式时间 并且兼容ie Date() 方法 只能这样写代码了，反正是因为后端没给时间改而增加的垃圾代码，此处有坑
        newDate = date.replace(/-/g, '/').replace('T', ' ').replace('.000+0000', '').replace('.000+0800', '');
        newDate = new Date(newDate);
    }

    return (newDate).getFullYear() +
            '-' +
            (((newDate).getMonth() + 1) > 9 ? ((newDate).getMonth() + 1) : ('0' + ((newDate).getMonth() + 1))) +
            '-' +
            ((newDate).getDate() > 9 ? (newDate).getDate() : ('0' + (newDate).getDate())) +
            ' ' +
            ((newDate).getHours() > 9 ? (newDate).getHours() : ('0' + (newDate).getHours())) +
            ':' +
            ((newDate).getMinutes() > 9 ? (newDate).getMinutes() : ('0' + (newDate).getMinutes())) +
            ':' +
            ((newDate).getSeconds() > 9 ? (newDate).getSeconds() : ('0' + (newDate).getSeconds()))
}

/*
    日期格式转换
    兼容 2020-03-26T09:28:25.000+0000 & 时间戳
*/
export const formatDate = (date) => {
    if (!date) {
        return '';
    }
    let newDate;
    if (typeof(date) == 'number') {
        newDate = new Date(date);
    } else if (typeof(date) == 'object') {
        newDate = date;
    } else {
        // 2020-03-26T09:28:25.000+0000
        // 2020-03-26T17:28:25.000+0800
        // 针对后台特殊格式时间 并且兼容ie Date() 方法 只能这样写代码了，反正是因为后端没给时间改而增加的垃圾代码，此处有坑
        newDate = date.replace(/-/g, '/').replace('T', ' ').replace('.000+0000', '').replace('.000+0800', '');
        newDate = new Date(newDate);
    }

    return (newDate).getFullYear() +
            '-' +
            (((newDate).getMonth() + 1) > 9 ? ((newDate).getMonth() + 1) : ('0' + ((newDate).getMonth() + 1))) +
            '-' +
            ((newDate).getDate() > 9 ? (newDate).getDate() : ('0' + (newDate).getDate()));
}
