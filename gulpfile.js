var gulp = require('gulp');
var mockServer = require('gulp-mock-server');
 
gulp.task("mock", function(){//配置热更新服务器

	gulp.src('.')
	  .pipe(mockServer({
	  	mockDir: './mock',
	    port: 8090
	  }));

})
 
/*wacth和server命令只能运行一个，所以可以用default同时执行多个任务，命令行直接gulp执行*/
gulp.task("default", gulp.series("mock", function(){}));//函数可以不传