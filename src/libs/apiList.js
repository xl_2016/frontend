/**
 * api请求列表
 * create by zhangguangde
 *
 */

let prefix = '';
const apiList = {

	// start -----------------------------------------------------------
	// 测试
	test: prefix + '/test',
	// 登录
	login: prefix + '/login',
	// 登出
	logout: prefix + '/:userName/logout',
	// 0001 列表
	trans_hist_info: prefix + '/:userName/trans_hist/hist_info',
	// 0001 broker 下拉框加载
	trans_hist_tz_broker: prefix + '/trans_hist/tz_broker',
	// 0001 上传数据
	trans_hist_user_add: prefix + '/:userName/trans_hist/user_add',
	// 0001 获取上传后的数据
	trans_hist_user_add_mt4_comment: prefix + '/:userName/trans_hist/user_add/mt4_comment',
	// 0001 提交 上传后获取的数据
	trans_hist_user_add_mod_comment: prefix + '/:userName/trans_hist/user_add/mod_comment',
	// 0001 删除
	trans_hist_delete: prefix + '/:userName/trans_hist/delete',
	// 0001 更新
	trans_hist_update: prefix + '/:userName/trans_hist/update',
	// 0002 提交0001选择的数据
	portfolio: prefix + '/:userName/portfolio',
	// 0002 列表数据
	portfolio_port_info: prefix + '/:userName/portfolio/port_info',
	// 0002 提示用户过滤掉的一些策略
	portfolio_filt_out: prefix + '/:userName/portfolio/filt_out',
	// 0002 分析
	portfolio_gen_graph: prefix + '/:userName/portfolio/gen_graph',
	// 0002 图表数据
	portfolio_ea_sym_gain: prefix + '/:userName/portfolio/ea_sym_gain',
	portfolio_ea_sym_dd: prefix + '/:userName/portfolio/ea_sym_dd',
	portfolio_sym_hour: prefix + '/:userName/portfolio/sym_hour',
	// 0002 回测
	portfolio_backtest: prefix + '/:userName/portfolio/backtest',
	// 0002 回测 表格 数据
	portfolio_backtest_stats: prefix + '/:userName/portfolio/backtest_stats',
	// 0002 回测 表格 数据 下载
	portfolio_backtest_report: prefix + '/:userName/portfolio/backtest_report',
	// 0002 回测 回测曲线图 数据
	portfolio_cumsum_dd: prefix + '/:userName/portfolio/cumsum_dd',
	// 0002 优化
	portfolio_run_opt: prefix + '/:userName/portfolio/run_opt',

	


	// end -------------------------------------------------------------

}

export default apiList
